#!/usr/bin/env python3

from flask import Flask, request, send_from_directory
from functools import wraps
from datetime import datetime
from random import choice
from os import listdir as ls


app = Flask(__name__)


@app.route('/')
def serve_quality_meme():
    with open('./access-log.txt', 'a+') as f:
        f.write('{} | {}\n'.format(
            str(datetime.now()),
            request.headers['X-Forwarded-For'] if 'X-Forwarded-For' in request.headers else request.remote_addr
        ))
        f.close()

    meme = choice(ls('./memes'))
    return send_from_directory('./memes', meme)


if __name__ == '__main__':
    app.run(
        '0.0.0.0',
        5000
    )
