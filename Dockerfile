FROM python:3.8-alpine

WORKDIR /opt/app

RUN pip3 install flask gunicorn

COPY . .

#CMD gunicorn -b 0.0.0.0:5000 -w 8 app:app
CMD python3 app.py
